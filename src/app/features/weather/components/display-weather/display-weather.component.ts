import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { IWeatherDetails } from '../../../../core/model/weather.interfaces';
import { WeatherServiceService } from '../../../../core/services/weather-service.service';

@Component({
  selector: 'app-display-weather',
  templateUrl: './display-weather.component.html',
})
export class DisplayWeatherComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription = new Subscription();
  private unitsOfDisplay: string[] = ['&#8451', '&#8457;'];
  public description: string;
  public loading = true;
  public location: string;
  public selectedUnitOfDisplay = this.unitsOfDisplay[0];
  public temperature: number;
  public temperatureInCelsius: number;
  public temperatureInFahrenheit: number;
  public windSpeed: string;

  constructor(private activatedRoute: ActivatedRoute, private weatherServiceService: WeatherServiceService) {
    this.location = this.activatedRoute.snapshot.params.location;
  }

  ngOnInit() {
    this.getWeather(this.location);
  }

  private getWeather(location: string): void {
    this.subscriptions.add(
      this.weatherServiceService
        .getWeather(location)
        .pipe(
          finalize(() => {
            this.loading = false;
          })
        )
        .subscribe(
          (weatherDetails: IWeatherDetails) => this.setWeatherDetails(weatherDetails),
          (error: HttpErrorResponse) => console.warn('error', error)
        )
    );
  }

  public toggleUnit(): void {
    this.selectedUnitOfDisplay =
      this.selectedUnitOfDisplay === this.unitsOfDisplay[0] ? this.unitsOfDisplay[1] : this.unitsOfDisplay[0];

    if (this.selectedUnitOfDisplay === this.unitsOfDisplay[0]) {
      this.temperature = this.temperatureInCelsius;
    } else {
      this.temperatureInFahrenheit = this.temperature * 9.0 / 5.0 + 32;
      this.temperature = this.temperatureInFahrenheit;
    }
  }

  private setWeatherDetails(weatherDetails): void {
    this.description = weatherDetails.weather[0].description;
    this.windSpeed = weatherDetails.wind.speed;
    this.temperatureInCelsius = weatherDetails.main.temp;
    this.temperature = this.temperatureInCelsius;
  }

  /**
   * Unsubscribe hook to ensure no memory leaks
   */
  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
