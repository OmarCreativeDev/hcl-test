import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { DisplayWeatherComponent } from './components/display-weather/display-weather.component';
import { WeatherPageComponent } from './pages/weather-page/weather-page.component';
import { WeatherRoutingModule } from './weather-routing.module';

@NgModule({
  declarations: [WeatherPageComponent, DisplayWeatherComponent],
  imports: [CommonModule, SharedModule, WeatherRoutingModule],
})
export class WeatherModule {}
