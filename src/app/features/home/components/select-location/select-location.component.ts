import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-select-location',
  templateUrl: './select-location.component.html',
})
export class SelectLocationComponent implements OnInit {
  public form: UntypedFormGroup;
  public cities: string[] = ['London', 'Birmingham', 'Manchester'];

  constructor(private router: Router, private formBuilder: UntypedFormBuilder) {}

  ngOnInit() {
    this.form = this.formBuilder.group({
      city: null,
    });
  }

  public onChange(city: string): void {
    if (city !== 'null') {
      this.router.navigate(['/weather', city]);
    }
  }
}
