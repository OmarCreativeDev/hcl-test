import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IWeatherDetails } from '../model/weather.interfaces';

@Injectable({
  providedIn: 'root',
})
export class WeatherServiceService {
  private appUrl = 'http://api.openweathermap.org/data/2.5/weather';
  private appId = '57c669c4d4cf062a87efc6957fdba4ab';

  constructor(private http: HttpClient) {}

  public getWeather(location: string): Observable<IWeatherDetails> {
    return this.http.get<IWeatherDetails>(`${this.appUrl}?q=${location},uk&appid=${this.appId}&units=metric`);
  }
}
